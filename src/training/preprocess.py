import pandas as pd
import os
from src.training.commons import ideal_reserve_price_for_non_prime
from src.utils.log_helper import log_exception, initialize_log
from src.training.features import RESERVE_VARIABLES, TARGET_OPTIMAL_RESERVE_COLUMN_NAME
import csv
import threading
import datetime


def preprocess_data(csv_fname='dec_data'):
    '''
    Argument:
        - csv_fname. corresponding csv file

    This function going to produce #timestamps fils, say for Nov 1-30 there will be 30 files, 
    each file contain corresponding data and calculated optimal price
    '''
    # load the data
    df = pd.read_csv(os.path.dirname(__file__) + "/../data/" + csv_fname + ".csv")
    # convert the datetimestamp to datetime
    df['datetimestamp'] = df['datetimestamp'].apply(lambda dt: datetime.datetime.strptime(dt, "%Y-%m-%d %H:%M:%S").date())
    dates_in_data = df['datetimestamp'].unique()

    for temp_date in dates_in_data:
        # initialize csv writer
        processed_data_file_name = os.path.dirname(__file__) + "/../data/processed_data_base_on_date/" + csv_fname + "_processed_"+ str(temp_date) + ".csv"
        processed_data_file_handler = open(processed_data_file_name, 'w')
        writer = csv.writer(processed_data_file_handler)
        writer.writerow(['correlationid'] + RESERVE_VARIABLES + [TARGET_OPTIMAL_RESERVE_COLUMN_NAME])

        # get the data of one specific date
        temp_df = df[df['datetimestamp']==temp_date]

        # group by all the feature columns, get a list of bid price for each acution
        qslist_corId_dict = temp_df.groupby(['correlationid'] + RESERVE_VARIABLES)['qualityscore'].apply(list).to_dict()
        reserve_corId_list = []

        for key in qslist_corId_dict:
            try:
                reserve_price = ideal_reserve_price_for_non_prime(qslist_corId_dict[key])
                reserve_corId_list.append({"correlationid": key[0], TARGET_OPTIMAL_RESERVE_COLUMN_NAME: reserve_price})
                writer.writerow(list(key) + [reserve_price])
            except Exception:
                # there can be some correlationId contain more than 6 quality score
                log_exception()

        # close file handler
        processed_data_file_handler.close()

        # convert to data frame
        reserve_corId_df = pd.DataFrame(reserve_corId_list)

        # merge with origin data
        temp_df = temp_df.merge(reserve_corId_df, on='correlationid', how='inner')

        # save to csv file
        processed_data_file_name = os.path.dirname(__file__) + "/../data/processed_data_base_on_date/" + csv_fname + "_processed_evaluation_"+ str(temp_date) + ".csv"
        temp_df.to_csv(processed_data_file_name)


if __name__ == "__main__":
    initialize_log()
    preprocess_data()
