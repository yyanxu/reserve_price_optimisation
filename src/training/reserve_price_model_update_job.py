import logging
from typing import List, Dict

import numpy as np
import pandas as pd

from src.optimisation.feature_encoder import FeatureEncoder
from src.training.bayes_linear import fit_bayes_linear, NormalGammaParams
from src.training.commons import safe_dot, prediction_metrics
from src.training.features import RESERVE_FEATURE_VARIABLES, REGULARIZATION, RESERVE_VARIABLES
from src.training.optimal_reserve_price import get_optimal_reserve_price
from src.utils.log_helper import initialize_log
from src.utils.time_helper import log_time_cost

DATA_SPLIT_TESTING_RATIO = 0.3
ENABLE_RANDOM_FEATURE_INCLUSION = False
DEFAULT_FEATURE_INCLUSION_PROB = 0.2
TRAINING_LOOK_BACK_DAYS = 1
MODEL_NAME = "reserve"


@log_time_cost
def reserve_model_update(selected_training_data: pd.DataFrame, targetcol: str):
    print("selected_training_data.shape", selected_training_data.shape)
    X_train, y_train, weights_train = extract_feature(selected_training_data.to_dict('record'), varcols=RESERVE_VARIABLES, targetcol=targetcol, weightcol='weights')
    reserve_model = train_reserve_model(X_train, y_train, weights_train)
    reserve_model_params = {'feature-variables': RESERVE_FEATURE_VARIABLES,
                            'feature-mappings': reserve_model['feature_encoder'].mappings,
                            'w': reserve_model['w'].tolist()}
    log_reserve_model_accuracy(X_train, y_train, weights_train, reserve_model, data_type="training")
    return reserve_model_params


def extract_feature(dat, varcols, targetcol, weightcol):
    y = list([x[targetcol] for x in dat])
    w = list([x[weightcol] for x in dat])
    X = list([{varcol: x[varcol] for varcol in varcols} for x in dat])
    return X, y, w


@log_time_cost
def train_reserve_model(X_train, y_train, weights_train):
    feature_encoder = FeatureEncoder(RESERVE_FEATURE_VARIABLES)
    feature_encoder.append(X_train, enable_random_feature_inclusion=ENABLE_RANDOM_FEATURE_INCLUSION,
                           prob=DEFAULT_FEATURE_INCLUSION_PROB, weights=weights_train)

    X = feature_encoder.transform(X_train)
    params = NormalGammaParams()
    params.w = np.zeros(X.shape[1])
    params.L = np.repeat(REGULARIZATION, X.shape[1])
    params.a = 1.0
    params.b = 1.0

    params = fit_bayes_linear(np.array(y_train), X, params, weights=weights_train, solver='L-BFGS-B')
    return {
        "feature_encoder": feature_encoder,
        "w": params.w
    }


def predict_reserve_price(X, reserve_model):
    X_transformed = reserve_model['feature_encoder'].transform(X)
    return safe_dot(X_transformed, reserve_model['w'])


def log_reserve_model_accuracy(X, y, weights, reserve_model, data_type="testing"):
    X_transformed = reserve_model['feature_encoder'].transform(X)
    y_pred = safe_dot(X_transformed, reserve_model['w'])

    group_by_columns = RESERVE_VARIABLES
    y_true_agg, y_pred_agg, weights_agg = aggregate_records_with_same_features(X, y, y_pred, weights, group_by_columns=group_by_columns)

    print({
        "data_type": data_type,
        "num_records": len(X),
        "sum_weights": float(np.sum(weights)),
        "raw_accuracy": prediction_metrics(y, y_pred, weights),
        "accuracy": prediction_metrics(y_true_agg, y_pred_agg, weights=weights_agg)
    })


def aggregate_records_with_same_features(X: List[Dict], y_true: List, y_pred: List, weights: List = None, group_by_columns=[]):
    # use the same group method in roktimon to measure accuracy
    model_acc_pd = pd.DataFrame.from_records(X)

    model_acc_pd = model_acc_pd.assign(y_true=pd.to_numeric(y_true),
                                       y_pred=pd.to_numeric(y_pred),
                                       weight=pd.to_numeric(weights))

    model_acc_pd['weighted_y_true'] = model_acc_pd['y_true'] * model_acc_pd['weight']
    model_acc_pd['weighted_y_pred'] = model_acc_pd['y_pred'] * model_acc_pd['weight']
    model_group_pd = model_acc_pd.groupby(group_by_columns).agg({'weighted_y_true': 'sum',
                                                                 'weighted_y_pred': 'sum',
                                                                 'weight': 'sum'})

    model_group_pd['average_y_true'] = model_group_pd['weighted_y_true'] / model_group_pd['weight']
    model_group_pd['average_y_pred'] = model_group_pd['weighted_y_pred'] / model_group_pd['weight']
    return model_group_pd['average_y_true'].values, model_group_pd['average_y_pred'].values, model_group_pd['weight'].values


if __name__ == '__main__':
    initialize_log()
    reserve_model_update()
