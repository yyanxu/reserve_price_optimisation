from multiprocessing import set_start_method

from sklearn.model_selection import GridSearchCV
from sklearn.preprocessing import LabelEncoder, OneHotEncoder
from xgboost import XGBRegressor

from src.training.commons import prediction_metrics
from src.training.feature_encoder import FeatureEncoder
from src.training.features import RESERVE_VARIABLES
from src.training.reserve_price_model_update_job import extract_feature, log_reserve_model_accuracy, \
    aggregate_records_with_same_features
from src.utils.time_helper import log_time_cost
from typing import List, Dict

import numpy as np
import pandas as pd
import os


@log_time_cost
def non_linear_reserve_model_update(selected_training_data: pd.DataFrame, targetcol: str):
    print("selected_training_data.shape", selected_training_data.shape)
    X_train, y_train, weights_train = extract_feature(selected_training_data.to_dict('record'), varcols=RESERVE_VARIABLES, targetcol=targetcol, weightcol='weights')
    encoder = FeatureEncoder(RESERVE_VARIABLES)
    encoder.fit(X_train)
    transformed_X_train = encoder.transform(X_train)
    xgb_model = model_update(transformed_X_train, y_train, weights_train)
    nonlinear_reserve_model_accuracy(X=X_train, y=y_train, weights=weights_train, y_pred=xgb_model.predict(transformed_X_train), data_type="training")
    return {"feature_encoder": encoder,
            "model": xgb_model}


@log_time_cost
def model_update(X_train, y_train, weights_train):
    set_start_method("forkserver", force=True)
    n_jobs = 2
    os.environ["OMP_NUM_THREADS"] = str(n_jobs)

    params = {
        'max_depth': 300,
        'n_estimators': 200,
        # 'colsample_bytree': 0.75,
        # 'subsample': 0.75,
        # 'reg_alpha': 0.05,
        # 'reg_lambda': 1.0,
        'n_jobs': n_jobs
    }
    reserve_model = XGBRegressor(**params)
    # param_test1 = {
    #     'learning_rate':[0.001, 0.01, 0.1, 1.0]
    #
    # }
    # gsearch1 = GridSearchCV(estimator=reserve_model, param_grid = param_test1, cv=3, verbose=10)
    # gsearch1.fit(X_train, y_train)
    # print(gsearch1.best_params_)
    # print(gsearch1.best_score_)

    reserve_model.fit(X_train, y_train, weights_train)
    return reserve_model


def nonlinear_reserve_model_accuracy(X, y, weights, y_pred, data_type="testing"):
    group_by_columns = RESERVE_VARIABLES
    y_true_agg, y_pred_agg, weights_agg = aggregate_records_with_same_features(X, y, y_pred, weights, group_by_columns=group_by_columns)

    print({
        "data_type": data_type,
        "num_records": len(X),
        "sum_weights": float(np.sum(weights)),
        "raw_accuracy": prediction_metrics(y, y_pred, weights),
        "accuracy": prediction_metrics(y_true_agg, y_pred_agg, weights=weights_agg)
    })