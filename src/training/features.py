from src.optimisation.feature_encoder import full_factorial_features

ACCURACY_MEASURE_VARIABLES = ['targetgroupid']

TARGET_OPTIMAL_RESERVE_COLUMN_NAME = 'optimalreservequalityscore'

# Click Model
RESERVE_VARIABLES = ['publisherid',
                     'agegroup',
                     'gender',
                     'deviceplatform',
                     'placement',
                     'hourofday']

RESERVE_SKIP_VARIABLES = [('publisherid', 'publisherwidgetid'),
                          ('publisherid', 'verticalid'),
                          ('publisherid', 'subverticalid'),
                          ('verticalid', 'subverticalid'),
                          ('verticalid', 'publisherwidgetid'),
                          ('subverticalid', 'publisherwidgetid'),
                          ('verticalid', 'placement'),
                          ('subverticalid', 'placement'),
                          ('dayofweek', 'hourofday')
                          ]

RESERVE_FEATURE_VARIABLES = full_factorial_features(RESERVE_VARIABLES, skip=RESERVE_SKIP_VARIABLES)

REGULARIZATION = len(RESERVE_FEATURE_VARIABLES) * 0.1
