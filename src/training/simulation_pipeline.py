from datetime import timedelta

import pickle

from src.optimisation.my_plot import *
from src.optimisation.reserve_price_optimize import *
from src.training.nonlinear_reserve_price_model_update import non_linear_reserve_model_update
from src.training.reserve_price_model_update_job import *
from src.utils.log_helper import initialize_log


# MODEL = "LINEAR"
MODEL = "NONLINEAR"
NUM_DAYS_FOR_TRAINING = 5
TRAINING_DATA_RATIO = 0.1
EVALUATE_DATA_RATIO = 0.1


def simulation_pipeline():
    # start date
    start_date = datetime(2018, 12, 6)

    date_list = []
    benchmark_revenues_list = []
    smart_revenues_list = []
    optimal_revenues_list = []

    '''
    Use previous 7 days to train, and use current date data to test
    '''
    # initial_training_data = get_data_for_initial_model_training()
    # print("get_data_for_initial_model_training done")
    #
    # curr_model_param = reserve_model_initial(initial_training_data, TARGET_OPTIMAL_RESERVE_COLUMN_NAME)

    for i in range(5):
        # load preprocessed data
        current_date = start_date + timedelta(days=i)
        print("current_date", current_date)

        training_data = get_data_for_incremental_build(current_date)
        training_data = enforce_str(training_data)
        # training_data = training_data.sample(frac=TRAINING_DATA_RATIO)
        updated_training_data, _ = get_optimal_reserve_price(training_data)
        if MODEL == "LINEAR":
            reserve_model = reserve_model_update(updated_training_data, TARGET_OPTIMAL_RESERVE_COLUMN_NAME)
        else:
            reserve_model = non_linear_reserve_model_update(updated_training_data, TARGET_OPTIMAL_RESERVE_COLUMN_NAME)

        # get evaluation data
        data_path = os.path.dirname(__file__) + "/../data/processed_data_base_on_date/dec_data_processed_evaluation_" + str(current_date.date()) + ".csv"
        evaluate_data = pd.read_csv(data_path)
        evaluate_data = enforce_str(evaluate_data)
        # evaluate_data = evaluate_data.sample(frac=EVALUATE_DATA_RATIO)
        _, corId_reserve_evaluate_bound_map = get_optimal_reserve_price(evaluate_data)

        # evaluate 
        results_benchmark = evaluate_current_strategy(evaluate_data)
        if MODEL == "LINEAR":
            results_smart = evaluate_smart_reserve_with_model(evaluate_data, reserve_model)
        else:
            results_smart = evaluate_smart_reserve_with_non_linear_model(evaluate_data, reserve_model, corId_reserve_evaluate_bound_map)
        results_upper_bound = evaluate_upper_bound(evaluate_data, corId_reserve_evaluate_bound_map)

        date_list.append(current_date)
        benchmark_revenues_list.append(results_benchmark)
        smart_revenues_list.append(results_smart)
        optimal_revenues_list.append(results_upper_bound)

    plot_reserve_model(date_list,
                       smart=smart_revenues_list,
                       benchmark=benchmark_revenues_list,
                       optimal=optimal_revenues_list)


def enforce_str(training_data):
    training_data['publisherid'] = training_data['publisherid'].astype(str)
    return training_data.dropna()


def get_data_for_incremental_build(current_date):
    training_data = []
    for j in range(1, NUM_DAYS_FOR_TRAINING + 1):
        temp_date = current_date - timedelta(days=j)
        file_path = os.path.dirname(__file__) + "/../data/processed_data_base_on_date/dec_data_processed_evaluation_" + str(temp_date.date()) + ".csv"
        training_data.append(pd.read_csv(file_path))
    return pd.concat(training_data)


def plot_reserve_model(date_list, smart, benchmark, optimal):
    with open("date_list.pickle", "wb") as fp:
        pickle.dump(date_list, fp)

    with open("smart.pickle", "wb") as fp:
        pickle.dump(smart, fp)

    with open("benchmark.pickle", "wb") as fp:
        pickle.dump(benchmark, fp)

    with open("optimal.pickle", "wb") as fp:
        pickle.dump(optimal, fp)

    plot_revenue(date_list,
                 smart_revenues=[float(x['rokt_revenue_usd'] * 1.0 / EVALUATE_DATA_RATIO) for x in smart],
                 benchmark_revenues=[float(x['rokt_revenue_usd'] * 1.0 / EVALUATE_DATA_RATIO) for x in benchmark],
                 optimal_revenues=[float(x['rokt_revenue_usd'] * 1.0 / EVALUATE_DATA_RATIO) for x in optimal],
                 plot_name="total_revenue_usd")

    all_publishers = smart[0]['publisher_revenue_usd'].keys()
    for publisherid in all_publishers:
        plot_revenue(date_list,
                     smart_revenues=[float(x['publisher_revenue_usd'].get(publisherid, 0) * 1.0 / EVALUATE_DATA_RATIO) for x in smart],
                     benchmark_revenues=[float(x['publisher_revenue_usd'].get(publisherid, 0) * 1.0 / EVALUATE_DATA_RATIO) for x in benchmark],
                     optimal_revenues=[float(x['publisher_revenue_usd'].get(publisherid, 0) * 1.0 / EVALUATE_DATA_RATIO) for x in optimal],
                     plot_name="publisher_{}".format(publisherid))


if __name__ == '__main__':
    initialize_log()
    simulation_pipeline()
