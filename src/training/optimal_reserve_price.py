from collections import defaultdict

from src.optimisation.reserve_price_optimize import evaluate_reserve_price
from src.training.commons import ideal_reserve_price_for_non_prime,calculate_revenue_for_non_prime_bids
from src.training.features import RESERVE_VARIABLES, TARGET_OPTIMAL_RESERVE_COLUMN_NAME
from src.utils.log_helper import initialize_log, log_exception
import pandas as pd
import os
import numpy as np

from src.utils.time_helper import log_time_cost

MIN_QS_GAP = 0.005
QS_STEP = 0.005
# GRANULARITY_QS = 500


# get_optimal_reserve_price_for_a_list_of_auctions with min and max optimal reserve price
def get_optimal_reserv_price_with_auctions(auctions, data, min_rqs, max_rqs):
    '''
    Argument:
        - auctions, a list of auctions, each auction is a list of biddings
        - min_rqs, lower bound of reserve quality score
        - max_rqs, upper bound of reserve quality score
    '''
    max_revenue = float("-inf")
    max_revenue_reserve_qs = min_rqs

    if max_rqs <= min_rqs + MIN_QS_GAP:
        return (max_rqs + min_rqs) / 2.0

    # print(min_rqs, max_rqs)
    for rqs in np.arange(min_rqs, max_rqs, QS_STEP):
        temp_revenue = 0

        for auction in auctions:
            temp_revenue += calculate_revenue_for_non_prime_bids(rqs, auction)

        # temp_revenue = get_expected_revenue_using_rqs(data, rqs)
        # print("rqs", rqs, rqs",temp_revenue", temp_revenue)
        if temp_revenue > max_revenue:
            max_revenue = temp_revenue
            max_revenue_reserve_qs = rqs
    return max_revenue_reserve_qs


def get_expected_revenue_using_rqs(data, rqs):
    data.loc[:, 'revenue_usd'] = data.apply(
        lambda x: evaluate_reserve_price(x['qualityscore'], rqs, x['auctionprice'], x['engagementrate'], x['maxbidprice'], x['currencyratetousd']), axis=1)
    return data['revenue_usd'].sum()


@log_time_cost
def get_optimal_reserve_price(data):
    '''
    Argument:
        - data, a data frame contain the data we need to extract

    Return:
        - dataFrame, each row contain features and corresponding optimal reserve quality score

    This function contain multiple dictionary which is confusing, list the key and values here, 
    should update the code after roktathon if going to extend this as a project

    Dictionaries:
    --name--                        --key--                 --value--        
    corId_qslist_map                correalationId          [quality scores], corresponding to 1 auction
    corId_opt_reserve_map           correalationId          optimal reserve quality score for that auction
    featurelist_corId_dict          a set of features       [correlationIds], there can be multiple auctions with same feature setting
    feature_opt_reservelist_dict    a set of features       [optimal reserve quality socre corresponding to the correlationIds in above dictionary]
    feature_optReserve_dict         a set of features       optimal reserve quality socre for a specific feature setting
    '''
    # a dictionary of list, key is correlationId, value is a list of quality score
    corId_qslist_map = data.groupby('correlationid')['qualityscore'].apply(list).to_dict()
    # optimal reserve price for each correlationid
    corId_opt_reserve_map = {}
    for correlationid in corId_qslist_map:
        try:
            reserve_price = ideal_reserve_price_for_non_prime(corId_qslist_map[correlationid])
            corId_opt_reserve_map[correlationid] = reserve_price
        except Exception:
            # there can be some correlationId contain more than 6 quality score
            log_exception()

    featurelist_corId_dict = data.groupby(RESERVE_VARIABLES)['correlationid'].apply(list).to_dict()
    feature_opt_reservelist_dict = defaultdict(list)  # feature_opt_reservelist_dict, certain feature set as key, and a list of optimal reserve price

    feature_opt_reserve_dict = dict()
    feature_corId_count_dict = dict()
    corId_reserve_bound_map = dict()
    for key in featurelist_corId_dict:
        auctions = []
        cor_Ids = set([])
        for corId in featurelist_corId_dict[key]:
            feature_opt_reservelist_dict[key].append(corId_opt_reserve_map[corId])
            auctions.append(corId_qslist_map[corId])
            cor_Ids.add(corId)

        if len(feature_opt_reservelist_dict[key]) == 1:
            optimal_reserve_for_feature_setting = feature_opt_reservelist_dict[key][0]
        else:
            optimal_reserve_for_feature_setting = \
                get_optimal_reserv_price_with_auctions(auctions,
                                                       data.loc[data['correlationid'].isin(cor_Ids), :],
                                                       min(feature_opt_reservelist_dict[key]),
                                                       max(feature_opt_reservelist_dict[key]))

        feature_opt_reserve_dict[key] = optimal_reserve_for_feature_setting
        feature_corId_count_dict[key] = len(featurelist_corId_dict[key])
        
        for corId in featurelist_corId_dict[key]:
            corId_reserve_bound_map[corId] = optimal_reserve_for_feature_setting


    # find the reserve price that optimize overall revenue of certain feature set
    # feature_opt_reserve_dict = dict()
    # feature_corId_count_dict = dict()
    # corId_reserve_bound_map = dict()

    # for features in featurelist_corId_dict:
    #     auctions = []
    #     cor_Ids = set([])
    #     for corId in featurelist_corId_dict[features]:
    #         auctions.append(corId_qslist_map[corId])
    #         cor_Ids.add(corId)

    #     if len(feature_opt_reservelist_dict[features]) == 1:
    #         optimal_reserve_for_feature_setting = feature_opt_reservelist_dict[features][0]
    #     else:
    #         optimal_reserve_for_feature_setting = \
    #             get_optimal_reserv_price_with_auctions(auctions,
    #                                                    data.loc[data['correlationid'].isin(cor_Ids), :],
    #                                                    min(feature_opt_reservelist_dict[features]),
    #                                                    max(feature_opt_reservelist_dict[features]))

    #     feature_opt_reserve_dict[features] = optimal_reserve_for_feature_setting
    #     feature_corId_count_dict[features] = len(featurelist_corId_dict[features])
    #     for corId in featurelist_corId_dict[features]:
    #         corId_reserve_bound_map[corId] = optimal_reserve_for_feature_setting

    # convert it to data frame
    df_list = []
    for key in feature_opt_reserve_dict:
        df_list.append(list(key) +[feature_opt_reserve_dict[key]] + [feature_corId_count_dict[key]])
    updated_df = pd.DataFrame(df_list, columns=RESERVE_VARIABLES+[TARGET_OPTIMAL_RESERVE_COLUMN_NAME]+['weights'])

    return updated_df, corId_reserve_bound_map


if __name__ == "__main__":
    initialize_log()
    get_optimal_reserve_price(pd.read_csv(os.path.dirname(__file__) + "/../data/processed_data_base_on_date/nov_data_processed_evaluation_2018-11-11.csv", nrows=300))