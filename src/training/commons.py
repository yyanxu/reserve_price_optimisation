import numpy as np
from scipy.sparse import issparse
from sklearn import metrics


def safe_dot(a, b, dense=False):
    if issparse(a) or issparse(b):
        result = a * b
        if dense and issparse(result):
            return result.todense()
        return result
    return np.dot(a, b)


def safe_mult(a, b, dense=False):
    if issparse(a):
        result = a.multiply(b)
    elif issparse(b):
        result = b.multiply(a)
    else:
        return a * b
    if dense:
        return result.todense()
    return result.tocsr()


def exp_cdf(X, we, window):
    lambda_ = np.exp(np.clip(safe_dot(X, we), -10., 10.))
    return 1. - np.exp(-1. * safe_mult(lambda_, window))


# exp(10)/(1 + exp(10)) = 0.9999546021312976
EXP_CLIPPING = 10.0


def logistic_prob(X, w):
    """ MAP (Bayes point) logistic regression probability with overflow prevention via exponent truncation.
    The logistic function is calculated as $\sigma_w(X) = \frac{e^{X w}}{1 + e^{X w}}$.

    Parameters
    ----------
    X : array-like, shape (N, p)
        Feature matrix
    w : array-like, shape (p, )
        Parameter vector

    Returns
    -------

    pr : array-like, shape (N, )
        vector of logistic regression values

    References
    ----------
    Chapter 8 of Murphy, K. 'Machine Learning a Probabilistic Perspective', MIT Press (2012)
    Chapter 4 of Bishop, C. 'Pattern Recognition and Machine Learning', Springer (2006)

    """
    pr = np.exp(np.clip(safe_dot(X, w), -EXP_CLIPPING, EXP_CLIPPING))
    return pr / (1. + pr)


def g_logistic_prob(X, w):
    """
    Gradient of logistic function w.r.t parameters w. Formally,
    $\nabla_w \sigma_w(X) = X \frac{e^{X w}}{(1 + e^{X w})^2}$

    Parameters
    ----------
    X : array-like, shape (N, p)
        Feature matrix
    w : array-like, shape (p, )
        Parameter vector

    Returns
    -------

    \nabla : array-like, shape (N, )
        vector of gradient values
    """
    pr = np.exp(np.clip(safe_dot(X, w), -EXP_CLIPPING, EXP_CLIPPING))
    return safe_mult(X, (pr / np.square(1. + pr))[:,np.newaxis])


def prediction_metrics(var_y, var_y_pred, weights):
    """
    Basic prediction metrics for a continuous variable with weighted samples.

    Parameters
    ----------
    var_y : numpy array, the true values
    var_y_pred : numpy array, the predicted values
    weights : corresponding weights

    Returns
    -------

    dict : dictionary with the following keys representing various metrics, all of them calculated
        using a weighted average
        - avg : average of true values
        - pred_avg : average of predicted values
        - bias2 : squared bias
        - var : variance
        - square_error : overall L2 error, i.e. average of (Y - Pred_Y)^2
        - abs_err : overall L1 error, i.e. average of |Y - Pred_Y|
        - abs_err_ratio : abs_err as percentage of true average (metric of reference)
        - r2_score
    """
    avg = np.average(var_y, weights=weights)
    pred_avg = np.average(var_y_pred, weights=weights)
    bias2 = np.average(var_y - var_y_pred, weights=weights) ** 2
    var = np.average(var_y_pred ** 2, weights=weights) - np.average(var_y_pred, weights=weights) ** 2
    square_error = metrics.mean_squared_error(var_y, var_y_pred, weights)
    abs_err = metrics.mean_absolute_error(var_y, var_y_pred, weights)
    r2_score = metrics.r2_score(var_y, var_y_pred, weights)
    return dict(avg=avg, pred_avg=pred_avg, bias2=bias2, var=var, square_error=square_error, abs_err=abs_err, abs_err_ratio=abs_err/avg, r2_score=r2_score)


def calculate_revenue_for_non_prime_bids(reserve_price, quality_score_bids):
    '''
    This is a universal revenue calculation, given reserver price and bids 
    '''
    valid_bids = [qsb for qsb in quality_score_bids if qsb >= reserve_price]
    valid_bids = sorted(valid_bids, reverse=True)
    valid_bids.append(reserve_price)

    if len(valid_bids) > 7:
        valid_bids = valid_bids[:7]
        
    revenue = 0

    for paid_price in valid_bids[1:]:
        revenue += paid_price

    return revenue


def ideal_reserve_price_for_non_prime(winner_bids_list):
    '''
    Calcuate ideal reserve price given the winner biddings in non prime auction

    Parameters:
    ---------
    winner_bids_list: variable number of non prime winner bids, should less than or equal to 6 bids.

    Return:
    ---------
    Ideal reserve price    
    '''
    if len(winner_bids_list) > 6:
        raise Exception("The amount of non prime winner biddings should less than or equal 6 {}".format(winner_bids_list))

    ideal_reserve_price = 0
    ideal_max_revenue = 0
    for rprice in winner_bids_list:
        temp_revenue = calculate_revenue_for_non_prime_bids(rprice, winner_bids_list)
        if temp_revenue > ideal_max_revenue:
            ideal_max_revenue = temp_revenue
            ideal_reserve_price = rprice
        elif temp_revenue == ideal_reserve_price and rprice < ideal_reserve_price:
            # when there are multiple optimal reserve price, I think we should take
            # the smaller one as ideal reserve price.
            ideal_reserve_price == rprice

    return ideal_reserve_price


