from __future__ import absolute_import, print_function
import numpy as np
from scipy.optimize import minimize

from src.training.commons import safe_dot, safe_mult


class NormalGammaParams:
    def __init__(self):
        self.w = None  # the weights of the normal component
        self.L = None  # the precision matrix of the weights
        self.a = None  # the shape parameter of the gamma component
        self.b = None  # the rate parameter of the gamma component


def f_log_posterior(w, w0, L, y, X, weights=None):
    """Returns negative log posterior probability.

    Parameters
    ----------
    w : array-like, shape (p, )
        vector of parameters at which the negative log posterior is to be evaluated
    w0 : array-like, shape (p, )
        vector of prior means on the parameters to be fit
    L : array-like, shape (p, p) or (p, )
        precision matrix of the weights
    y : array-like, shape (N, )
        vector of target values
    X : array-like, shape (N, p)
        array of features
    weights : array-like, shape (N, )
        vector of data point weights

    Returns
    -------
    neg_log_post : float
                negative log posterior probability

    References
    ----------
    Chapters 2 and 3 of Bishop, C. 'Pattern Recognition and Machine Learning', Springer (2006)

    """

    # fill in weights if need be
    if weights is None:
        weights = np.ones(len(np.atleast_1d(y)), )
    if len(np.atleast_1d(weights)) != len(np.atleast_1d(y)):
        raise ValueError('weight vector must be same length as response vector')

    # calculate negative log posterior

    if len(L.shape) == 2:
        prior = 0.5 * np.dot((w - w0).T, np.dot(L, (w - w0)))
    else:
        prior = 0.5 * np.dot((w - w0).T, L * (w - w0))

    likelihood = 0.5 * np.sum(np.square(y - safe_dot(X, w)))

    return float(prior + likelihood)


def g_log_posterior_small(w, w0, L, y, X, weights=None):
    """Returns normalized (to 1) gradient of the negative log posterior probability.
    This is used for BFGS and L-BFGS-B solvers which tend to not converge unless
    the gradient is normalized.

    Parameters
    ----------
    w : array-like, shape (p, )
        vector of parameters at which the negative log posterior is to be evaluated
    w0 : array-like, shape (p, )
        vector of prior means on the parameters to be fit
    L : array-like, shape (p, p) or (p, )
        precision matrix of the weights
    y : array-like, shape (N, )
        vector of target values
    X : array-like, shape (N, p)
        array of features
    weights : array-like, shape (N, )
        vector of data point weights

    Returns
    -------
    grad_log_post : array-like, shape (p, )
                normalized (to 1) gradient of negative log posterior

    References
    ----------
    Chapters 2 and 3 of Bishop, C. 'Pattern Recognition and Machine Learning', Springer (2006)

    """

    # fill in weights if need be
    if weights is None:
        weights = np.ones(len(np.atleast_1d(y)), )
    if len(np.atleast_1d(weights)) != len(np.atleast_1d(y)):
        raise ValueError('weight vector must be same length as response vector')

    # calculate gradient

    if len(L.shape) == 2:
        prior = np.dot(L, (w - w0))
    else:
        prior = L * (w - w0)

    residuals = y - safe_dot(X, w)
    likelihood = safe_dot(residuals, X)

    grad_log_post = prior - likelihood

    # normalize gradient to length 1
    grad_log_post = grad_log_post / np.sqrt(np.sum(np.square(grad_log_post)))

    return grad_log_post


def fit_bayes_linear(y, X, prior, weights=None, solver='least-squares', bounds=None, maxiter=100):
    """ Bayesian Linear Regression Solver.  Assumes Normal-Gamma conjugate prior over the mean
    and precision of the target variable. Uses scipy.optimize.minimize.

    Parameters
    ----------
    y : array-like, shape (N, )
        array of binary {0,1} responses
    X : array-like, shape (N, p)
        array of features
    prior : NormalGammaParams
        prior normal-gamma params
    weights : array-like, shape (N, )
        array of data point weights
    solver : string
        scipy optimize solver used.  this should be either 'least-squares', 'BFGS' or 'L-BFGS-B'.
        The default is least-squares.
    bounds : iterable of length p
        a length p list (or tuple) of tuples each of length 2.
        This is only used if the solver is set to 'L-BFGS-B'. In that case, a tuple
        (lower_bound, upper_bound), both floats, is defined for each parameter.  See the
        scipy.optimize.minimize docs for further information.
    maxiter : int
        maximum number of iterations for scipy.optimize.minimize solver.

    Returns
    -------
    posterior: NormalGammaParams
        the normal gamma params of the posterior fit

    References
    ----------
    Chapters 2 and 3 of Bishop, C. 'Pattern Recognition and Machine Learning', Springer (2006)

    """

    # Check that dimensionality of inputs agrees

    # check X
    if len(X.shape) != 2:
        raise ValueError('X must be a N*p matrix')
    (nX, pX) = X.shape

    # check y
    if len(y.shape) > 1:
        raise ValueError('y must be a vector of shape (p, )')
    if len(np.atleast_1d(y)) != nX:
        raise ValueError('y and X do not have the same number of rows')

    # check wprior
    if len(prior.w.shape) > 1:
        raise ValueError('prior should be a vector of shape (p, )')
    if len(np.atleast_1d(prior.w)) != pX:
        raise ValueError('prior mean has incompatible length')

    # check H
    if len(prior.L.shape) == 1:
        if np.atleast_1d(prior.L).shape[0] != pX:
            raise ValueError('prior Hessian is diagonal but has incompatible length')
    elif len(prior.L.shape) == 2:
        (h1, h2) = np.atleast_2d(prior.L).shape
        if h1 != h2:
            raise ValueError('prior Hessian must either be a p*p square matrix or a vector or shape (p, ) ')
        if h1 != pX:
            raise ValueError('prior Hessian is square but has incompatible size')

    # fill in weights if need be
    if weights is None:
        weights = np.ones(len(np.atleast_1d(y)), )
    if len(np.atleast_1d(weights)) != len(np.atleast_1d(y)):
        raise ValueError('weight vector must be same length as response vector')

    # Do the regression

    n = float(len(y))
    posterior = NormalGammaParams()
    if len(prior.L.shape) == 2:
        posterior.L = prior.L + safe_dot(X.T, X)
    else:
        posterior.L = prior.L + np.squeeze(np.asarray(np.sum(safe_mult(X.T, X.T), axis=1)))

    if solver == 'least-squares':
        if len(prior.L.shape) == 2:
            posterior.w = np.dot(np.linalg.inv(posterior.L), np.dot(prior.L, prior.w) + safe_dot(X.T, y))
        else:
            XTy = safe_dot(X.T, y)
            posterior.w = np.array([(1. / posterior.L[i]) * (prior.L[i] * prior.w[i] + XTy[i])
                                    for i in range(prior.L.shape[0])])
    elif solver == 'BFGS':
        ww = minimize(f_log_posterior, prior.w,
                      args=(prior.w, prior.L, y, X, weights),
                      jac=g_log_posterior_small,
                      method='BFGS', options={'maxiter': maxiter})
        posterior.w = ww.x
    elif solver == 'L-BFGS-B':
        ww = minimize(f_log_posterior, prior.w,
                      args=(prior.w, prior.L, y, X, weights),
                      jac=g_log_posterior_small, method='L-BFGS-B',
                      bounds=bounds, options={'maxiter': maxiter})
        posterior.w = ww.x
    else:
        raise ValueError('Unknown solver specified: "{0}"'.format(solver))

    posterior.a = prior.a + (0.5 * n)
    posterior.b = prior.b + (0.5 * np.sum(np.square(y - safe_dot(X, posterior.w))))

    return posterior
