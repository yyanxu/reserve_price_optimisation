import numpy as np
import scipy.sparse as sp
from itertools import combinations


def full_factorial_features(base_variables, skip=[]):
    skip = [set(x) for x in skip]
    base_variables = sorted(base_variables)
    full_variables = list(combinations(base_variables, 1))
    return list(filter(lambda x: set(x) not in skip, full_variables))


class FeatureEncoder:
    """Transforms lists of feature-value mappings to vectors.

    This transformer turns lists of mappings (dict-like objects) of feature
    names to feature values in scipy.sparse matrices.

    Features are expected to be categorical. Feature values will be
    dummy coded (aka 1-of-c encoding).

    Feature values will include independent features as well as conjunctive
    features (feature interactions) of degree 2. An intercept feature will
    also be included.
    """

    def __init__(self, feature_variables, mappings=None):
        self.feature_variables = feature_variables
        if mappings is None:
            self.mappings = {}
        else:
            self.mappings = dict(mappings)

    def _feature_combinations(self, x):
        features = []
        for fvar in self.feature_variables:
            features.append((fvar, x[fvar]))
        return features

    def fit(self, X):
        for x in X:
            for k in self._feature_combinations(x):
                if k not in self.mappings:
                    self.mappings[k] = len(self.mappings) + 1
        return self

    def transform(self, X):
        indices = []
        indptr = [0]
        for x in X:
            indices.append(0)
            for k in self._feature_combinations(x):
                if k in self.mappings:
                    indices.append(self.mappings[k])
                else:
                    print("feature not found")
                    return None
            indptr.append(len(indices))
        shape = (len(indptr) - 1, len(self.mappings) + 1)
        return sp.csr_matrix(([1]*len(indices), indices, indptr),
                             shape=shape, dtype=np.float64)