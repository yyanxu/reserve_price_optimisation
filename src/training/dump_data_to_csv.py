from src.utils.data_loader import get_db_connection, get_paid_price_data, get_paid_price_data_no_split
import pandas as pd
import os
from datetime import datetime, timedelta


start_date = datetime(year=2018, month=12, day=1, hour=0, minute=0, second=0)
end_date = datetime(year=2018, month=12, day=10, hour=23, minute=59, second=59)


def load_data():
    # Step 1: set up connections
    db_client = get_db_connection()
    db_client.connect()

    # Step 2: load training and testing data set
    dataset = get_paid_price_data_no_split(db_client.get_conn(),
                                           start_time=start_date,
                                           end_time=end_date)

    pd.DataFrame(dataset).to_csv(os.path.dirname(__file__) + "/../data/dec_data.csv", index=False)


if __name__ == '__main__':
    load_data()
