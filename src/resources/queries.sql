{% sql 'get_auction_data' %}
select
    rac.datetimestamp,
    rac.correlationid,
    rac.publisherid,
    rac.placement,
    rac.qualityscore,
    rac.engagementrate,
    coalesce(rac.expectedclickrevenue, rac.maxbidprice) as maxbidprice,
    rac.auctionprice,
    coalesce(rac.minqualityscore, 0) as minqualityscore,
    rac.publisherid,
    rac.publisherwidgetid,
    rac.publisherwidgetgroupid,
    rac.agegroup,
    rac.gender,
    rac.deviceplatform,
    rac.placement,
    case when fr.revenue_usd > 0 then fr.revenue_local / fr.revenue_usd else 1.0 end as currencyratetousd
from roktmlauctioncreatives rac
    join targetgroupreferralcreativeassociation tgrca on tgrca.referralcreativeid = rac.referralcreativeid and tgrca.targetgroupid = rac.targetgroupid
    join fact_responses fr on fr.memberid = rac.memberid and tgrca.creativeid = fr.adcreativeid and fr.conversions > 0 and rac.placement = fr.placement
    where rac.placement != 32
    and rac.datetimestamp between %(startdate)s and %(enddate)s
    and rac.shown = True
    and producttype != 'optimize'
{% endsql %}


{% sql 'get_paid_price_data' %}
with eligiablecorrelationids as (
    select distinct rac.correlationid
    from roktmlauctioncreatives rac
    join targetgroupreferralcreativeassociation tgrca on tgrca.referralcreativeid = rac.referralcreativeid and tgrca.targetgroupid = rac.targetgroupid
    join fact_responses fr on fr.memberid = rac.memberid and tgrca.creativeid = fr.adcreativeid and fr.conversions > 0 and rac.placement = fr.placement
    where rac.placement != 32
    and rac.datetimestamp between %(startdate)s and %(enddate)s
), correlationidsplit as (
    select *,
    random() <= %(test_ratio)s as is_test
    from eligiablecorrelationids
)
select
    cs.is_test,
    cs.correlationid,
    rac.publisherwidgetid,
    rac.publisherwidgetgroupid,
    rac.publisherid,
    rac.gender,
    rac.agegroup,
    rac.deviceplatform,
    rac.placement,
    rac.minqualityscore,
    rac.qualityscore,
    rac.rankinresponse,
    rac.auctionprice,
    coalesce(rac.expectedclickrevenue, rac.maxbidprice) as maxbidprice,
    rac.engagementrate
from roktmlauctioncreatives rac
join correlationidsplit cs on rac.correlationid = cs.correlationid
where rac.shown = True and producttype != 'optimize'
{% endsql %}


{% sql 'get_paid_price_data_no_split' %}
with eligiablecorrelationids as (
    select distinct rac.correlationid
    from roktmlauctioncreatives rac
    join targetgroupreferralcreativeassociation tgrca on tgrca.referralcreativeid = rac.referralcreativeid and tgrca.targetgroupid = rac.targetgroupid
    join fact_responses fr on fr.memberid = rac.memberid and tgrca.creativeid = fr.adcreativeid and fr.conversions > 0 and rac.placement = fr.placement
    where rac.placement = 32
    and rac.datetimestamp between %(startdate)s and %(enddate)s
)
select
    cs.correlationid,
    si.parentid as verticalid,
    si.industryid as subverticalid,
    rac.publisherwidgetid,
    rac.publisherid,
    rac.gender,
    rac.agegroup,
    rac.deviceplatform,
    rac.placement,
    rac.minqualityscore,
    rac.qualityscore,
    rac.rankinresponse,
    rac.auctionprice,
    coalesce(rac.expectedclickrevenue, rac.maxbidprice) as maxbidprice,
    rac.engagementrate,
    extract(hour from rac.datetimestamp) as hourofday,
    extract(dayofweek from rac.datetimestamp) as dayofweek,
    case when fr.revenue_usd > 0 then fr.revenue_local / fr.revenue_usd else 1.0 end as currencyratetousd,
    rac.datetimestamp
from roktmlauctioncreatives rac
join eligiablecorrelationids cs on rac.correlationid = cs.correlationid
join targetgroupreferralcreativeassociation tgrca on tgrca.referralcreativeid = rac.referralcreativeid and tgrca.targetgroupid = rac.targetgroupid
join fact_responses fr on fr.memberid = rac.memberid and tgrca.creativeid = fr.adcreativeid and fr.conversions > 0 and rac.placement = fr.placement
left join tblpublisherwidget pw ON pw.publisherwidgetid = rac.publisherwidgetid
left join tblpublishersite tps on pw.publishersiteid = tps.publishersiteid
left join tblindustry si on tps.industryid = si.industryid
where rac.shown = True and rac.producttype != 'optimize' and rac.placement = 32
    and rac.datetimestamp between %(startdate)s and %(enddate)s
    and coalesce(rac.expectedclickrevenue, rac.maxbidprice) > 0
{% endsql %}
