from typing import List
import os

import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt


def plot_revenue(times: List, smart_revenues: List, benchmark_revenues: List, optimal_revenues: List = None, plot_name: str=""):
    if len(times) != len(smart_revenues):
        return
    if len(times) != len(benchmark_revenues):
        return

    plt.figure(figsize=(12, 9))
    plt.plot(times, smart_revenues, '-*', label='challenger: revenue_of_smart_reserve')
    plt.plot(times, benchmark_revenues, '-*', label='benchmark: revenue_of_ROKT_minQS')

    if optimal_revenues is not None:
        plt.plot(times, optimal_revenues, '-*', label='upbound: revenue_of_perfect_reserve_model')
    plt.xlabel('Time')
    plt.ylabel('Revenue (USD)')
    plt.ylim(bottom=0)
    plt.legend(loc='upper right')
    plt.tight_layout()
    plt.savefig(os.path.dirname(__file__) + "/../data/prime_revenue_comparison_{}.png".format(plot_name))
