import numpy as np
import scipy.sparse as sp
from itertools import combinations
from random import random


def full_factorial_features(base_variables, skip=[]):
    skip = [set(x) for x in skip]
    base_variables = sorted(base_variables)
    full_variables = list(combinations(base_variables, 1)) + list(combinations(base_variables, 2))
    return list(filter(lambda x: set(x) not in skip, full_variables))


class FeatureEncoder:
    """Transforms lists of feature-value mappings to vectors.

    This transformer turns lists of mappings (dict-like objects) of feature
    names to feature values in scipy.sparse matrices.

    Features are expected to be categorical. Feature values will be
    dummy coded (aka 1-of-c encoding).

    Feature values will include independent features as well as conjunctive
    features (feature interactions) of degree 2. An intercept feature will
    also be included.
    """

    def __init__(self, feature_variables, mappings=None):
        self.feature_variables = feature_variables
        if mappings is None:
            self.mappings = {}
        else:
            self.mappings = dict(mappings)

    def _feature_combinations(self, x):
        features = []
        for fvar in self.feature_variables:
            feat = tuple([(var, x[var]) for var in fvar])
            features.append(feat)
        return features

    def append(self, X, enable_random_feature_inclusion=False, prob=1.0, weights=None):
        if enable_random_feature_inclusion:
            if weights is None:
                weights = np.ones(len(X))
            for x, w in zip(X, weights):
                accumulated_prob = 1.0 - np.power((1.0 - prob), w)
                for k in self._feature_combinations(x):
                    if k not in self.mappings:
                        if random() < accumulated_prob:
                            self.mappings[k] = len(self.mappings) + 1
        else:
            for x in X:
                for k in self._feature_combinations(x):
                    if k not in self.mappings:
                        self.mappings[k] = len(self.mappings) + 1
        return self

    def transform(self, X):
        indices = []
        indptr = [0]
        for x in X:
            indices.append(0)
            for k in self._feature_combinations(x):
                if k in self.mappings:
                    indices.append(self.mappings[k])
            indptr.append(len(indices))
        shape = (len(indptr) - 1, len(self.mappings) + 1)
        return sp.csr_matrix(([1]*len(indices), indices, indptr),
                             shape=shape, dtype=np.float64)

    def adjust_hyperparams(self, w, H, regularisation=1.0):
        """Given a weight vector `w` and a diagonal Hessian matrix `H`,
        reshape them such that they match the shape of the FeatureEncoder
        by appending zeros to `w` and appending values of `regularisation` to `H`.

        It is assumed that len(self.mappings) >= len(w) + 1.
        """
        diff = len(self.mappings) + 1 - len(w)
        if len(w) != len(H):
            raise ValueError('len(w) must equal len(H)')
        if diff < 0:
            raise ValueError('len(self.mappings) must be >= len(w) + 1')
        w = np.concatenate((w, np.zeros(diff)), axis=0)
        H = np.concatenate((H, np.full(diff, regularisation)), axis=0)
        return w, H
