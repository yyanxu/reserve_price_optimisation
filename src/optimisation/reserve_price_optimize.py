import functools

from src.training.features import TARGET_OPTIMAL_RESERVE_COLUMN_NAME, RESERVE_VARIABLES
from src.utils.data_loader import ingest_db_connection, get_auction_data
from datetime import datetime
import pandas as pd
import numpy as np
import os

start_date = datetime(year=2018, month=11, day=21, hour=0, minute=0, second=0)
end_date = datetime(year=2018, month=11, day=24, hour=0, minute=0, second=0)


@ingest_db_connection
def start_reserve_price_optimization(db_client):
    # evaluation_data = get_auction_data(db_conn=db_client.get_conn(),
    #                                 start_time=start_date,
    #                                 end_time=end_date)
    # evaluation_data.to_csv(os.path.dirname(__file__) + "/../data/evaluation_data.csv", index=False)

    evaluation_data = pd.read_csv(os.path.dirname(__file__) + "/../data/evaluation_data.csv")
    results_current = evaluate_current_strategy(evaluation_data)
    print(results_current["rokt_revenue_usd"])

    results_smart = evaluate_smart_reserve(evaluation_data)
    print(results_smart["rokt_revenue_usd"])


def evaluate_current_strategy(auction_data: pd.DataFrame):
    auction_data.loc[:, 'revenue_usd'] = auction_data.apply(lambda x: evaluate_reserve_price(x['qualityscore'], x['minqualityscore'], x['auctionprice'], x['engagementrate'], x['maxbidprice'], x['currencyratetousd']), axis=1)
    return {
        "rokt_revenue_usd": auction_data['revenue_usd'].sum(),
        "publisher_revenue_usd": auction_data.groupby('publisherid')['revenue_usd'].sum()
    }


def evaluate_reserve_price(qualityscore, minqualityscore, auctionprice, engagementrate, maxbidprice, currencyratetousd):
    # print({"qualityscore": qualityscore,
    #        "minqualityscore": minqualityscore,
    #        "(auctionprice - 0.01) * engagementrate": (auctionprice - 0.01) * engagementrate,
    #        "auctionprice": auctionprice,
    #        "(minqualityscore / qualityscore * maxbidprice + 0.01)": (minqualityscore / qualityscore * maxbidprice + 0.01)})

    if (qualityscore == 0) or (qualityscore < minqualityscore):
        return 0.0
    # if (auctionprice - 0.01) * engagementrate >= minqualityscore:
    #     return auctionprice * currencyratetousd
    if (auctionprice - 0.01) * qualityscore / maxbidprice >= minqualityscore:
        return auctionprice * currencyratetousd
    return (minqualityscore / qualityscore * maxbidprice + 0.01) * currencyratetousd


def evaluate_smart_reserve(auction_data: pd.DataFrame):
    auction_data.loc[:, 'smartreserve'] = auction_data.apply(lambda x: calculate_smart_reserve(x), axis=1)
    auction_data.loc[:, 'revenue_usd'] = auction_data.apply(lambda x: evaluate_reserve_price(x['qualityscore'], x['smartreserve'], x['auctionprice'], x['engagementrate'], x['maxbidprice'], x['currencyratetousd']), axis=1)
    return {
        "rokt_revenue_usd": auction_data['revenue_usd'].sum(),
        "publisher_revenue_usd": auction_data.groupby('publisherid')['revenue_usd'].sum()
    }


def calculate_smart_reserve(context, reserve_price_model_param= None):
    return linear_model(variables=reserve_price_model_param['feature-variables'],
                        feature_mappings=reserve_price_model_param['feature-mappings'],
                        weights=reserve_price_model_param['w'],
                        features={**context})


def linear_model(variables, feature_mappings, weights, features):
    term = weights[0]
    for fv in variables:
        k = tuple(map(lambda x: (x, features[x]), fv))
        if k in feature_mappings:
            term += weights[feature_mappings[k]]
    return term


def calculate_smart_reserve_non_linear(context, reserve_price_model_param=None, corId_reserve_evaluate_bound_map=None):
    X = [{varcol: context[varcol] for varcol in RESERVE_VARIABLES}]
    transformed_X_train = reserve_price_model_param['feature_encoder'].transform(X)
    try:
        output = reserve_price_model_param['model'].predict(transformed_X_train)
        return output[0] - 0.01
    except:
        return context['minqualityscore']


def calculate_upbound(corId_reserve_bound_map, x):
    return evaluate_reserve_price(x['qualityscore'], corId_reserve_bound_map.get(x['correlationid']), x['auctionprice'], x['engagementrate'], x['maxbidprice'], x['currencyratetousd'])


def evaluate_upper_bound(auction_data: pd.DataFrame, corId_reserve_bound_map):
    upbound_fun = functools.partial(calculate_upbound, corId_reserve_bound_map)
    auction_data.loc[:, 'revenue_usd'] = auction_data.apply(lambda x: upbound_fun(x), axis=1)
    return {
        "rokt_revenue_usd": auction_data['revenue_usd'].sum(),
        "publisher_revenue_usd": auction_data.groupby('publisherid')['revenue_usd'].sum()
    }


def evaluate_smart_reserve_with_model(auction_data: pd.DataFrame, reserve_price_model_param):
    auction_data.loc[:, 'smartreserve'] = auction_data.apply(lambda x: calculate_smart_reserve(x, reserve_price_model_param), axis=1)
    auction_data.loc[:, 'revenue_usd'] = auction_data.apply(lambda x: evaluate_reserve_price(x['qualityscore'], x['smartreserve'], x['auctionprice'], x['engagementrate'], x['maxbidprice'], x['currencyratetousd']), axis=1)
    return {
        "rokt_revenue_usd": auction_data['revenue_usd'].sum(),
        "publisher_revenue_usd": auction_data.groupby('publisherid')['revenue_usd'].sum()
    }


def evaluate_smart_reserve_with_non_linear_model(auction_data: pd.DataFrame, reserve_price_model_param, corId_reserve_evaluate_bound_map):
    auction_data.loc[:, 'smartreserve'] = auction_data.apply(
        lambda x: calculate_smart_reserve_non_linear(x, reserve_price_model_param, corId_reserve_evaluate_bound_map), axis=1)
    auction_data.loc[:, 'revenue_usd'] = auction_data.apply(
        lambda x: evaluate_reserve_price(x['qualityscore'], x['smartreserve'], x['auctionprice'], x['engagementrate'],
                                         x['maxbidprice'], x['currencyratetousd']), axis=1)
    return {
        "rokt_revenue_usd": auction_data['revenue_usd'].sum(),
        "publisher_revenue_usd": auction_data.groupby('publisherid')['revenue_usd'].sum()
    }


if __name__ == '__main__':
    start_reserve_price_optimization()