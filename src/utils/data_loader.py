import os
import pandas as pd
import psycopg2

from functools import wraps
from datetime import datetime
from configparser import ConfigParser
from src.utils.exceptions import DatabaseConnectionException
from src.utils.snaql_queries import exec_query, queries


class DatabaseAccessClient:
    def __init__(self, user: str, password: str, host: str, port: str, dbname: str, sslmode: str, sslrootcert: str):
        self._user = user
        self._password = password
        self._host = host
        self._port = port
        self._dbname = dbname
        self._sslmode = sslmode
        self._sslrootcert = sslrootcert
        self._conn = None

    def connect(self):
        try:
            self._conn = psycopg2.connect(host=self._host, port=self._port, user=self._user, password=self._password,
                                          dbname=self._dbname, sslmode=self._sslmode, sslrootcert=self._sslrootcert)
        except psycopg2.Error:
            raise DatabaseConnectionException("Unable to connect {}!".format(self._host))

    def get_conn(self):
        return self._conn

    def close(self):
        if self._conn:
            self._conn.close()


def get_configuration():
    conf = ConfigParser()
    conf.read(os.path.dirname(__file__) + '/../../config.ini')
    return conf


def get_db_connection(db_server: str = 'redshift'):
    conf = get_configuration()
    return DatabaseAccessClient(user=conf[db_server]['user'],
                                password=conf[db_server]['password'],
                                host=conf[db_server]['host'],
                                port=conf[db_server]['port'],
                                dbname=conf[db_server]['dbname'],
                                sslmode=conf[db_server]['sslmode'],
                                sslrootcert=conf[db_server]['sslrootcert'])


def ingest_db_connection(f):
    @wraps(f)
    def wrapper(*args, **kwds):
        db_client = get_db_connection()
        db_client.connect()
        results = f(db_client, *args, **kwds)
        return results
    return wrapper


def get_auction_data(db_conn, start_time: datetime, end_time: datetime):
    params = {'startdate': start_time,
              'enddate': end_time}
    return pd.DataFrame(exec_query(db_conn, queries.get_auction_data(), params))


def get_paid_price_data(db_conn, start_time: datetime, end_time: datetime, test_ratio: float):
    params = {'startdate': start_time,
              'enddate': end_time,
              'test_ratio': test_ratio}

    training_data, test_data = [], []
    for row in exec_query(db_conn, queries.get_paid_price_data(), params):
        if row['is_test']:
            test_data.append(row)
        else:
            training_data.append(row)
    return training_data, test_data


def get_paid_price_data_no_split(db_conn, start_time: datetime, end_time: datetime):
    params = {'startdate': start_time,
              'enddate': end_time}
    return exec_query(db_conn, queries.get_paid_price_data_no_split(), params)
