import logging
import json


class JSONFormatter(logging.Formatter):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.not_used_attrs = set(["exc_info", "exc_text", "created", "msecs", "process", "processName", "stack_info",
                                   "thread", "threadName", "levelname", "levelno", "args", "filename", "relativeCreated"])

    def format(self, record: logging.LogRecord):
        record_dict = record.__dict__
        record_dict = self._get_jsonable_dict(record_dict)

        msg_dict = {}
        for attr_name in record_dict:
            if attr_name not in self.not_used_attrs:
                msg_dict[attr_name] = record_dict[attr_name]

        if "asctime" not in msg_dict:
            msg_dict["asctime"] = self.formatTime(record)

        return json.dumps(msg_dict)

    # resolve the issue: OPTIM-356 keys must be a string
    def _get_jsonable_dict(self, record_dict):
        new_dict = {}
        for attr_name in record_dict:
            if isinstance(attr_name, str):
                new_attr_name = attr_name
            else:
                new_attr_name = str(attr_name)

            if isinstance(record_dict[attr_name], dict):
                new_dict[new_attr_name] = self._get_jsonable_dict(record_dict[attr_name])
            else:
                new_dict[new_attr_name] = record_dict[attr_name]
        return new_dict
