import boto3
import os
import pickle
import pandas as pd
from typing import List, Tuple
from src.utils.exceptions import OptimusModelNotExistingException

OPTIMUS_VERSION = 1.1
INDEX_SEPARATOR = "\n"
REDSHIFT_DELIMITER = '|'
MODEL_INDEX_KEY = "optimus_params/{}/{}/latest_model_params.pickle"
LOCAL_TEMP_FILE = os.path.dirname(__file__) + "/temp_{}.pickle"


def get_latest_model_from_s3(bucket_name: str, model_name: str):
    s3_key = _get_s3_key(bucket_name=bucket_name, model_name=model_name)
    local_model_file = LOCAL_TEMP_FILE.format(model_name)
    file_src_and_dst = [(s3_key, local_model_file)]
    download_s3_files_to_local(file_src_and_dst, bucket_name=bucket_name)
    return deserialize_model_params(local_model_file)


def _get_s3_key(bucket_name: str, model_name: str):
    model_s3_key = MODEL_INDEX_KEY.format(OPTIMUS_VERSION, model_name)
    try:
        s3_key_str = _read_s3_file(bucket_name=bucket_name, s3_key=model_s3_key)
        if not s3_key_str:
            raise OptimusModelNotExistingException("S3 index file {} is empty".format(model_s3_key))
        s3_keys = s3_key_str.split(INDEX_SEPARATOR)
        return s3_keys[-1] if len(s3_keys) > 0 else None
    except:
        raise OptimusModelNotExistingException("S3 file {} not existing".format(model_s3_key))


def _read_s3_file(bucket_name: str, s3_key: str):
    try:
        session = boto3.Session()
        s3conn = session.resource('s3')
        a_file = s3conn.Object(bucket_name, s3_key)
        return a_file.get()['Body'].read().decode('UTF-8')
    except:
        return None


def _fetch_bucket(bucket_name: str):
    session = boto3.Session()
    s3conn = session.resource('s3')
    return s3conn.Bucket(bucket_name)


def download_s3_files_to_local(file_src_and_dst: List[Tuple], bucket_name: str):
    bucket = _fetch_bucket(bucket_name)
    for src_path, dst_path in file_src_and_dst:
        bucket.download_file(src_path, dst_path)


def deserialize_model_params(file_path: str):
    with open(file_path, 'rb') as obj_file:
        return pickle.load(obj_file)

##
# File Upload & Download from S3
##
def upload_local_files_to_s3(file_src_and_dst: List[Tuple], bucket_name: str):
    bucket = _fetch_bucket(bucket_name)
    for src_path, dst_path in file_src_and_dst:
        bucket.upload_file(src_path, dst_path)


def download_s3_files_to_local(file_src_and_dst: List[Tuple], bucket_name: str):
    bucket = _fetch_bucket(bucket_name)
    for src_path, dst_path in file_src_and_dst:
        bucket.download_file(src_path, dst_path)


def read_s3_file_as_str(bucket_name: str, key: str):
    try:
        session = boto3.Session()
        s3conn = session.resource('s3')

        a_file = s3conn.Object(bucket_name, key)
        return a_file.get()['Body'].read().decode('UTF-8')
    except:
        return None


def _fetch_bucket(bucket_name: str):
    session = boto3.Session()
    s3conn = session.resource('s3')
    a_bucket = s3conn.Bucket(bucket_name)
    return a_bucket


def get_simulation_data(bucket_name, prefix):
    s3 = boto3.resource('s3')
    bucket = s3.Bucket(bucket_name)
    objs = list(bucket.objects.filter(Prefix=prefix))

    df_list = []
    for obj in objs:
        if obj.key.endswith('.bz2'):
            s3_path = "s3://{}/{}".format(obj.bucket_name, obj.key)
            try:
                df_list.append(pd.read_table(s3_path, header=None, sep=REDSHIFT_DELIMITER))
            except:
                #  skip empty file
                continue

    if len(df_list) == 0:
        return None
    return pd.concat(df_list, ignore_index=True)


def is_key_exist(bucket_name: str, s3_key: str):
    s3 = boto3.resource('s3')
    bucket = s3.Bucket(bucket_name)
    objs = list(bucket.objects.filter(Prefix=s3_key))
    if len(objs) > 0 and objs[0].key == s3_key:
        return True
    return False
