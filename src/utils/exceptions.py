# a number of error definitions


class DatabaseConnectionException(RuntimeError):
    pass


class FileNotExistingException(RuntimeError):
    pass


class NegativeBidPriceException(RuntimeError):
    pass


class FeatureMissingException(RuntimeError):
    pass


class OptimusModelNotExistingException(RuntimeError):
    pass