import sys
import traceback
import logging
import logging.handlers
import os
import numpy as np

from sklearn import metrics
from typing import List, Dict

from sklearn.metrics import roc_auc_score

from src.utils.JSONFormatter import JSONFormatter

LOG_FILE_PATH = os.path.dirname(__file__) + "/../../log/reserve_model.log"
LOG_EXCEPTION_FILE_PATH = os.path.dirname(__file__) + "/../../log/reserve_model_exceptions.log"
LOG_FILE_MAX_SIZE_BYTE = 1024 * 1024 * 1024  #1GiB
LOG_FILE_ROTATION_BACKUP = 1


def initialize_log():
    log_dir = os.path.dirname(LOG_FILE_PATH)
    if not os.path.exists(log_dir):
        os.makedirs(log_dir)

    logger = logging.getLogger()
    logger.setLevel(logging.DEBUG)

    file_handler = logging.handlers.RotatingFileHandler(LOG_FILE_PATH, maxBytes=LOG_FILE_MAX_SIZE_BYTE, backupCount=LOG_FILE_ROTATION_BACKUP)
    json_formatter = JSONFormatter()
    file_handler.setFormatter(json_formatter)
    logger.addHandler(file_handler)

    # second log just for exceptions without json format
    exp_logger = logging.getLogger('reserve_model_exceptions')
    exp_logger.setLevel(logging.ERROR)
    exp_file_handler = logging.handlers.RotatingFileHandler(LOG_EXCEPTION_FILE_PATH, maxBytes=LOG_FILE_MAX_SIZE_BYTE, backupCount=LOG_FILE_ROTATION_BACKUP)
    exp_formatter = logging.Formatter('%(asctime)s - %(name)s - %(message)s')
    exp_file_handler.setFormatter(exp_formatter)
    exp_logger.addHandler(exp_file_handler)


def log_exception():
    exp_logger = logging.getLogger('reserve_model_exceptions')
    exp_logger.error(traceback.format_exc())
