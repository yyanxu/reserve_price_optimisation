# README #
Context-based Reserve Price Optimisation

************ Prerequisites *****************
python3
aws-vault

************ install dependencies ************
```
$ virtualenv venv

$ . venv/bin/activate

$ pip install -r requirements.txt
```

************ run the code ************

# Step 1: train reserve price model
   python training/reserve_price_model_update_job.py

# Step 2: evaluate reserve price model
   python optimisation/reserve_price_optimize.py

*********** Run unit test ******************

`$ ~/reserve_price_optimisation$ python3 -m pytest test/Test_IdealReservePrice/Test_IdealReservePriceCalCulation.py`