import pytest
from src.training.commons import calculate_revenue_for_non_prime_bids, ideal_reserve_price_for_non_prime

@pytest.mark.parametrize("test_reserve_price, test_qs_bids, expected", [
    (5, [6,5,2], 10),
    (3, [4,5,6], 12),
    (5, [], 0),
    (5, [1,3,2], 0),
    (0.9, [7,6,5,4,3,2,1,0.5,0.4], 21)
])
def test_calculate_revenue_for_non_prime_bids(test_reserve_price,\
	test_qs_bids, expected):
    assert calculate_revenue_for_non_prime_bids(test_reserve_price, test_qs_bids) == expected


@pytest.mark.parametrize("test_qs_bids, expected", [
    ([6,1], 6),
    ([6,5,5.5], 5),
    ([6,1.0,0.9,0.8,0.7,1.0], 6),
    ([6,5,3,0.5,0.4], 3),
    ([], 0),
    ([1.0], 1.0)
])
def test_ideal_reserve_price_for_non_prime(test_qs_bids, expected):
    assert ideal_reserve_price_for_non_prime(test_qs_bids) == expected



